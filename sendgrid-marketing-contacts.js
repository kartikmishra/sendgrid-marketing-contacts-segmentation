const _ = require('lodash');
const axios = require('axios');

const sendgridMarketingContacts = async () => {


  //SENDGRID_CONTACTS_LIST_NAME == List name of the contacts in SENDGRID
  //SENDGRID_CONTACT_API_KEY == SENDGRID API KEY

  let dataToSend = [
      {
        email: 'kartikmishra@gmail.com',
        first_name: 'Kartik',
        last_name: 'Mishra',
      },
      {
        email: 'test1@gmail.com',
        first_name: 'Test 1',
        last_name: 'Test 2',
      },
      {
        email: 'test3@gmail.com',
        first_name: 'Test 3',
        last_name: 'Test 4',
      }
  ];

  const keyRes = await getSendgridCustomFeildsDefinition();

  const listIDRes = await getSendgridListInformation();

  let listId = [];
  let filteredListId = _.filter(
    listIDRes,
    (ele) => ele.name == process.env.SENDGRID_CONTACTS_LIST_NAME
  );
  listId.push(filteredListId[0].id);

  let keysMap = buildKeyMap(keyRes);
  //build the final JSON for SENDGRID
  var data = {
    list_ids: listId,
    contacts: dataToSend,
    json: true,
  };
  const options = {
    headers: {
      'Content-Type': 'application/json',
      'authorization': `Bearer ${process.env.SENDGRID_CONTACT_API_KEY}`,
    },
    method: 'put',
    url: 'https://api.sendgrid.com/v3/marketing/contacts',
    data: data,
  };

  try {
    const res = await axios(options);
    console.log('==== Contacts created ====', res.data);
  } catch (err) {
    console.log('=== this is error ====', err.response.data);
  }
};


//This method will map the custom field name with custom field ID fetch from SENDGRID Account
const buildKeyMap = (keyRes) => {
  let keysMap = {};
  for (var i = 0; i < keyRes.length; i++) {
    keysMap[keyRes[i].name] = keyRes[i].id;
  }

  return keysMap;
};

//This method will fetch all the custom fields from SENDGRID account for that SENDGRID_CONTACT_API_KEY 
const getSendgridCustomFeildsDefinition = async () => {
  let keyRes;
  try {
    keyRes = await axios({
      headers: {
        'Content-Type': 'application/json',
        'authorization': `Bearer ${process.env.SENDGRID_CONTACT_API_KEY}`,
      },
      method: 'GET',
      url: 'https://api.sendgrid.com/v3/marketing/field_definitions',
    });
    console.log(
      '==== Custom fields fetched successfully ====',
      JSON.stringify(keyRes.data.custom_fields)
    );
  } catch (err) {
    console.log(
      '=== Error occured while fetching custom fields ====',
      JSON.stringify(err)
    );
  }

  return keyRes.data.custom_fields;
};

//This method will fetch all the defined LIST from SENDGRID account for that SENDGRID_CONTACT_API_KEY
const getSendgridListInformation = async () => {
  let listIDRes;
  try {
    listIDRes = await axios({
      headers: {
        'Content-Type': 'application/json',
        'authorization': `Bearer ${process.env.SENDGRID_CONTACT_API_KEY}`,
      },
      method: 'GET',
      url: 'https://api.sendgrid.com/v3/marketing/lists?page_size=100',
    });
    console.log(
      '==== List ID fetched successfully ====',
      JSON.stringify(listIDRes.data.result)
    );
  } catch (err) {
    console.log(
      '=== Error occured while fetching List Details ====',
      JSON.stringify(err)
    );
  }

  return listIDRes.data.result;
};

module.exports =  sendgridMarketingContacts;
